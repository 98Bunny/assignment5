#include<stdio.h>
int noOfAttendees(int tktPrice);
int income(int tktPrice, int noOfAttendees);
int expenditure(int noOfAttendees);
int profit(int income, int expenditure);

int main()
{
	int NoOfAttendees, Income, Expenditure, Profit;
	for(int TktPrice = 10 ; TktPrice <= 30 ; TktPrice++)
	{
		NoOfAttendees = noOfAttendees(TktPrice);
		Income = income(TktPrice, NoOfAttendees);
		Expenditure = expenditure(NoOfAttendees);
		Profit = profit(Income, Expenditure);
		printf("\nTicket Price : Rs %d\nNo of Attendees : %d\nIncome : Rs %d\nExpenditure : Rs %d\nProfit : Rs %d\n", TktPrice, NoOfAttendees, Income, Expenditure, Profit);
	}
	return 0;
}

int noOfAttendees(int tktPrice)
{
	int noOfAttendees = 180 - 4 * tktPrice;
	return noOfAttendees;
}

int income(int tktPrice, int noOfAttendees)
{
	int income = tktPrice * noOfAttendees;
	return income;
}

int expenditure(int noOfAttendees)
{
	int expenditure = 500 + (noOfAttendees * 3);
	return expenditure;
}

int profit(int income, int expenditure)
{
	int profit = income - expenditure;
	return profit;
}
